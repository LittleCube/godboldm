/*
 * Code modified by MATTHEW GODBOLD
 * for CSCE 574 Assignment 4
 * Spring 2024
 */

#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/LaserScan.h"
#include <cstdlib> // Needed for rand()
#include <ctime> // Needed to seed random number generator with a time value

class PID {
	// PID controller for control calculations
	public:
		float kp;
		float kd;
		float curr_error = 0;
		float prev_error = 0;
		float curr_error_deriv = 0;
		float control = 0;
			
	double update(double current_error, double delta_t) {
		prev_error = curr_error;
		curr_error = current_error;
		curr_error_deriv = (curr_error - prev_error) / delta_t;
		control = kp * curr_error + kd * curr_error_deriv;
		return control;
	}
};
		
		
class controller {
	public:
	controller(ros::NodeHandle& nh) {
			srand(time(NULL));
			
			// Advertise a new publisher for the simulated robot's velocity command topic
			// (the second argument indicates that if multiple command messages are in
			//  the queue to be sent, only the last command will be sent)
			commandPub = nh.advertise<geometry_msgs::Twist>("cmd_vel", 1);
			
			// Subscribe to the simulated robot's laser scan topic and tell ROS to call
			// this->commandCallback() whenever a new message is published on that topic
			laserSub = nh.subscribe("base_scan", 1, &controller::commandCallback, this);
			
			pid = PID();
			pid.kp = kp;
			pid.kd = kd;
			
		};
	
	// Send a velocity command 
	void move(double linearVelMPS, double angularVelRadPS) {
		geometry_msgs::Twist msg;
		msg.linear.x = linearVelMPS;
		msg.angular.z = angularVelRadPS;
		commandPub.publish(msg);
	}
	
	// Process the incoming laser scan message
	void commandCallback(const sensor_msgs::LaserScan::ConstPtr& msg) {
		unsigned int minIndex = ceil((MIN_SCAN_ANGLE_RAD - msg->angle_min) / msg->angle_increment);
		unsigned int maxIndex = ceil((MAX_SCAN_ANGLE_RAD - msg->angle_min) / msg->angle_increment);
		float closestRange = msg->ranges[minIndex];
		float closestDirection = MIN_SCAN_ANGLE_RAD;
		
		for (unsigned int currIndex = minIndex + 1; currIndex < maxIndex; currIndex++) {
			if (msg->ranges[currIndex] < closestRange) {
				closestRange = msg->ranges[currIndex];
				closestDirection = msg->angle_min + currIndex * msg->angle_increment;
			}
		}
		
		if (closestRange > PROXIMITY_RANGE_M) {
			// move towards closest obstacle
			std::cout << "APPROACHING OBSTACLE" << std::endl;
			move(FORWARD_SPEED_MPS, closestDirection);
		
		} else if (d_last == -1.0) {
			// set the closest range and initial time when an obstacle is approached
			d_last = closestRange;
			rotateStartTime = ros::Time::now();
			
		} else {
			
			// follow the obstacle
			ros::Duration dt = ros::Time::now() - rotateStartTime;
			double error = closestRange - PROXIMITY_RANGE_M;
			double control = pid.update(error, dt.toSec());
			std::cout << "ERROR: " << error << "   CONTROL: " << control << std::endl;
			move(FORWARD_SPEED_MPS, control);
			d_last = closestRange;
			rotateStartTime = ros::Time::now();
		}
	};
	
	void spin() {
		ros::Rate rate(20); // rate in Hz
		
		while (ros::ok()) {								
			
			ros::spinOnce();
			rate.sleep();
			
		}
	};
	
	constexpr static double MIN_SCAN_ANGLE_RAD = -135.0/180*M_PI;
	constexpr static double MAX_SCAN_ANGLE_RAD = +135.0/180*M_PI;
	constexpr static float PROXIMITY_RANGE_M = 3; // Should be smaller than sensor_msgs::LaserScan::range_max
	constexpr static double FORWARD_SPEED_MPS = 1.0;
	constexpr static double ROTATE_SPEED_RADPS = M_PI/2;
	constexpr static double kp = 4.0;
	constexpr static double kd = 4.0;
	
	protected:
	ros::Publisher commandPub; // Publisher to the simulated robot's velocity command topic
	ros::Subscriber laserSub; // Subscriber to the simulated robot's laser scan topic
	ros::Time rotateStartTime; // Start time of the rotation
	ros::Duration rotateDuration; // Duration of the rotation
	PID pid;
	double d_last = -1.0;	
	
};

int main(int argc, char **argv) {
	ros::init(argc, argv, "random_walk"); // Initiate new ROS node named "random_walk"
	ros::NodeHandle n;
	controller walker(n); // Create new random walk object
	walker.spin(); // Execute FSM loop
	//walker.square(); // walk in a square;
	
	return 0;
};
				
		
