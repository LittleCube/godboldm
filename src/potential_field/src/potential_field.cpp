/*
 * Code modified by MATTHEW GODBOLD
 * for CSCE 574 Assignment 1
 * Spring 2024
 */

#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/LaserScan.h"
#include <vector>
#include <cstdlib> // Needed for rand()
#include <ctime> // Needed to seed random number generator with a time value
#include <tf/LinearMath/Quaternion.h> // Needed to convert rotation ...
#include <tf/LinearMath/Matrix3x3.h>  // ... quaternion into Euler angles


struct Pose {
  double x; // in simulated Stage units
  double y; // in simulated Stage units
  double heading; // in radians
  ros::Time t; // last received time
  
  // Construct a default pose object with the time set to 1970-01-01
  Pose() : x(0), y(0), heading(0), t(0.0) {};
  
  // Process incoming pose message for current robot
  void poseCallback(const nav_msgs::Odometry::ConstPtr& msg) {
    double roll, pitch;
    x = msg->pose.pose.position.x;
    y = msg->pose.pose.position.y;
    tf::Quaternion q = tf::Quaternion(msg->pose.pose.orientation.x, \
      msg->pose.pose.orientation.y, msg->pose.pose.orientation.z, \
      msg->pose.pose.orientation.w);
    tf::Matrix3x3(q).getRPY(roll, pitch, heading);
    t = msg->header.stamp;
  };
};


class PotFieldBot {
public:
  // Construst a new Potential Field controller object and hook up
  // this ROS node to the simulated robot's pose, velocity control,
  // and laser topics
  PotFieldBot(ros::NodeHandle& nh, int id, int n, \
      double gx, double gy) : ID(id), num_robots(n), \
      destX(gx), destY(gy) {
    // Initialize random time generator
    srand(time(NULL));

    // Advertise a new publisher for the current simulated robot's
    // velocity command topic (the second argument indicates that
    // if multiple command messages are in the queue to be sent,
    // only the last command will be sent)
    commandPub = nh.advertise<geometry_msgs::Twist>("cmd_vel", 1);

    // Subscribe to the current simulated robot's laser scan topic and
    // tell ROS to call this->laserCallback() whenever a new message
    // is published on that topic
    laserSub = nh.subscribe("base_scan", 1, &PotFieldBot::laserCallback, this);
    
    // Subscribe to each robot' ground truth pose topic
    // and tell ROS to call pose->poseCallback(...) whenever a new
    // message is published on that topic
    for (int i = 0; i < num_robots; i++) {
      pose.push_back(Pose());
    }
    for (int i = 0; i < num_robots; i++) {
		poseSubs.push_back(nh.subscribe("/base_pose_ground_truth", 1,
		&Pose::poseCallback, &pose[i]));
    }
  };


  // Send a velocity command
  void move(double linearVelMPS, double angularVelRadPS) {
    geometry_msgs::Twist msg; // The default constructor will set all commands to 0
    msg.linear.x = linearVelMPS;
    msg.angular.z = angularVelRadPS;
    commandPub.publish(msg);
  };


  // Process incoming laser scan message
  void laserCallback(const sensor_msgs::LaserScan::ConstPtr& msg) {
    // TODO: parse laser data
    // (see http://www.ros.org/doc/api/sensor_msgs/html/msg/LaserScan.html)
	
	// Keep track of laser readings and the angle of each
	// Will be used in potential_field function
	LaserAngles.clear();
	LaserReadings.clear();
	
	// Only use angles within the scan angle
	unsigned int minIndex = ceil((MIN_SCAN_ANGLE_RAD -msg->angle_min) / msg->angle_increment);
	unsigned int maxIndex = ceil((MAX_SCAN_ANGLE_RAD -msg->angle_min) / msg->angle_increment);
	
	for (unsigned int currIndex = minIndex+1; currIndex < maxIndex; currIndex++) {
		// std::cout << pose[0].heading + msg->angle_min + currIndex * msg->angle_increment << " , ";
		LaserAngles.push_back(msg->angle_min + currIndex * msg->angle_increment);
		LaserReadings.push_back(msg->ranges[currIndex]);
	}
	
	Pose_X.push_back(pose[0].x);
	Pose_Y.push_back(pose[0].y);
	if (Pose_X.size() > 2) {
		Pose_X.erase(Pose_X.begin());
	}
	if (Pose_Y.size() > 2) {
		Pose_Y.erase(Pose_Y.begin());
	}
	
  };
  
  
  geometry_msgs::Twist potential_field() {
	  // Potential field function to control robot
	  // Will return a geometry_msgs that can be passed
	  // into the move function
	  geometry_msgs::Twist vel;
	  
	  if (LaserAngles.size() == 0) {
		  // Do nothing if no readings are available
		  // Usually happens when robot is initialized
		  
		  return vel;
		  
	  } else {

	  double f_r_x, f_r_y, f_a;
    f_r_x = 0.0;
    f_r_y = 0.0;
    f_a = 0.0;
	  
	  // ATTRACTIVE
	  
    // FIXME: Below sections of equation in parentheses need to be squared
	  //f_r += gamma * std::pow((destX - pose[0].x), 2); // attractive x force
	  f_a += gamma * (std::pow((destX - pose[0].x), 2) + std::pow((destY - pose[0].y), 2)); // attractive force
						
						
	// REPULSIVE
	for (unsigned int j = 0; j !=  LaserReadings.size()-1; j++) {
		// std::cout << LaserReadings[j] << std::endl;
		if (LaserReadings[j] > d_safe + eps && LaserReadings[j] < beta) {
			// FIRST CASE: apply equation 1
			// alpha / (d_i - d_safe)^2
			
			// std::cout << "FIRST CASE" << std::endl;
			
      // FIXME: The below equations that use trig
      if ((LaserAngles[j] + pose[0].heading) > 2*M_PI) {

        f_r_x -= std::abs(alpha / pow(LaserReadings[j] * cos((LaserAngles[j] + pose[0].heading) - 2*M_PI) - d_safe, 2));
			  f_r_y -= std::abs(alpha / pow(LaserReadings[j] * sin((LaserAngles[j] + pose[0].heading) - 2*M_PI) - d_safe, 2));

      }
			f_r_x -= std::abs(alpha / pow(LaserReadings[j] * cos((LaserAngles[j] + pose[0].heading) - 2*M_PI) - d_safe, 2));
			f_r_y -= std::abs(alpha / pow(LaserReadings[j] * sin((LaserAngles[j] + pose[0].heading) - 2*M_PI) - d_safe, 2));

			
		} else if (LaserReadings[j] < d_safe + eps) {
			
			// std::cout << "SECOND CASE" << std::endl;
			
			f_r_x -= std::abs(alpha / pow(eps, 2) * cos((LaserAngles[j] + pose[0].heading) - 2*M_PI));
			f_r_y -= std::abs(alpha / pow(eps, 2) * sin((LaserAngles[j] + pose[0].heading) - 2*M_PI));
			
		} else {
			
			// std::cout << "THIRD CASE" << std::endl;
			//f_r += 0;
			//f_a += 0;
		}
	}
	
	double f_a_angle = atan2(destY - pose[0].y, destX - pose[0].x);
  double f_a_x = f_a*cos(f_a_angle);
  double f_a_y = f_a*sin(f_a_angle);
	//double sum_f = sqrt(pow(f_r, 2) + pow(f_a, 2));
  double sum_f_x = f_a_x + f_r_x;
  double sum_f_y = f_a_y + f_r_y;
  double sum_f_mag = sqrt(pow(sum_f_x, 2) + pow(sum_f_y, 2));
	double orientation = atan2(sum_f_y, sum_f_x);
	double ang_vel = (orientation - pose[0].heading);

	
	// Check if orientation is behind the robot
	if (abs(orientation - pose[0].heading) > 3*M_PI/4) {
		sum_f_mag = 0;
	}
	
	// Check if rotation speed is too high
	if (ang_vel > ROTATE_SPEED_RADPS) {
		ang_vel = ROTATE_SPEED_RADPS;
	}
	
	// Check if forward speed is too high
	if (sum_f_mag > FORWARD_SPEED_MPS) {
		sum_f_mag = FORWARD_SPEED_MPS;
	}
	
	// Check if robot is stuck
	if (Pose_X.size() > 1 && Pose_Y.size() > 1) {
		if (abs(Pose_X[0] - Pose_X[1]) < 1e-5 && abs(Pose_Y[0] - Pose_Y[1]) < 1e-5) {
			if (pose_tracker > 5) {
				std::cout << "STUCK" << std::endl;
        //sum_f = FORWARD_SPEED_MPS;
        sum_f_mag = FORWARD_SPEED_MPS;
				pose_tracker = 0;
			} else {
				pose_tracker ++;
			}
		}
	}
	
			
	vel.angular.z = ang_vel;
  vel.linear.x = sum_f_mag;
	std::cout << "ORIENTATION: " << orientation << "  ,  HEADING: " << pose[0].heading << std::endl;
	std::cout << "ANGLE: " << vel.angular.z << std::endl;
	std::cout << "LINEAR: " << vel.linear.x << std::endl;
	
	
	return vel;
	 }
  }
  
  
  // Main FSM loop for ensuring that ROS messages are
  // processed in a timely manner, and also for sending
  // velocity controls to the simulated robot based on the FSM state
  void spin() {
    ros::Rate rate(30); // Specify the FSM loop rate in Hz
	  double tol = 0.05;
	  double new_x, new_y;

    while (ros::ok()) { // Keep spinning loop until user presses Ctrl+C
      // TODO: remove demo code, compute potential function, actuate robot
   
	  
	  std::cout << std::endl;
	  std::cout << "Goal: " << destX << ", " << destY;
	  std::cout << std::endl;
	  std::cout << "Pose: " << pose[0].x << ", " << pose[0].y << ", " << pose[0].heading << std::endl;
	  
	  geometry_msgs::Twist vel = potential_field();
	  move(vel.linear.x, vel.angular.z);
	
      ros::spinOnce(); // Need to call this function often to allow ROS to process incoming messages
      rate.sleep(); // Sleep for the rest of the cycle, to enforce the FSM loop rate
	  
	  if (abs(pose[0].x - destX) < tol && abs(pose[0].y - destY) < tol) {
		  
		  move(0, 0);
		  std::cout << "ENTER A NEW GOAL:";
		  std::cin >> new_x >> new_y;
		  destX = new_x;
		  destY = new_y;
		  
	  } 
	  
    }
  };

  // Tunable motion controller parameters
  constexpr static double MIN_SCAN_ANGLE_RAD = -60.0/180*M_PI;
  constexpr static double MAX_SCAN_ANGLE_RAD = +60.0/180*M_PI;
  constexpr static double FORWARD_SPEED_MPS = 2.0;
  constexpr static double ROTATE_SPEED_RADPS = M_PI/2;
  constexpr static double gamma = 10;
  constexpr static double alpha = 1;
  constexpr static double beta = 2;
  constexpr static double eps = 0.05;
  constexpr static double d_safe = 2;
  int pose_tracker = 0;
  
  std::vector<double> LaserReadings;
  std::vector<double> LaserAngles;
  std::vector<double> Pose_X;
  std::vector<double> Pose_Y;
  
  

protected:
  ros::Publisher commandPub; // Publisher to the current robot's velocity command topic
  ros::Subscriber laserSub; // Subscriber to the current robot's laser scan topic
  std::vector<ros::Subscriber> poseSubs; // List of subscribers to all robots' pose topics
  std::vector<Pose> pose; // List of pose objects for all robots
  int ID; // 0-indexed robot ID
  int num_robots; // Number of robots, positive value
  double destX, destY; // Coordinates of goal
  ros::Duration rotateDuration; // Duration of the rotation
};


int main(int argc, char **argv) {
  int id = -1, num_robots = 0;
  double destX, destY;
  bool printUsage = false;
  
  // Parse and validate input arguments
  if (argc <= 4) {
    printUsage = true;
  } else {
    try {
      id = boost::lexical_cast<int>(argv[1]);
      num_robots = boost::lexical_cast<int>(argv[2]);
      destX = boost::lexical_cast<double>(argv[3]);
      destY = boost::lexical_cast<double>(argv[4]);

      if (id < 0) { printUsage = true; }
      if (num_robots <= 0) { printUsage = true; }
    } catch (std::exception err) {
      printUsage = true;
    }
  }
  if (printUsage) {
    std::cout << "Usage: " << argv[0] << " [id] [num_robots] [destX] [destY]" << std::endl;
    return EXIT_FAILURE;
  }
  
  ros::init(argc, argv, "potential_field");
  ros::NodeHandle n;
  PotFieldBot robbie(n, id, num_robots, destX, destY);
  robbie.spin();

  return EXIT_SUCCESS;
};

