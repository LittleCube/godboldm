# CMake generated Testfile for 
# Source directory: /home/godboldm/GodboldM/src
# Build directory: /home/godboldm/GodboldM/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("random_walk")
subdirs("grid_mapper")
subdirs("pid_controller")
subdirs("potential_field")
